import AsyncStorage from "@react-native-async-storage/async-storage";
import { createClient } from "@supabase/supabase-js";

// Better put your these secret keys in .env file
export const supabase = createClient("https://wihmocstpmwgqwwvdjtt.supabase.co", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6IndpaG1vY3N0cG13Z3F3d3ZkanR0Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2NTI3NjcwMTAsImV4cCI6MTk2ODM0MzAxMH0.4ZOGyUf6Cd2sbn4C-QZ2R-7vddWaZmQelAFkaylwUMM", {
  localStorage: AsyncStorage as any,
  detectSessionInUrl: false // Prevents Supabase from evaluating window.location.href, breaking mobile
});
