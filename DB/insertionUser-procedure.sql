CREATE DEFINER=`root`@`%` PROCEDURE `insertionUser`(in nombre varchar(45),in apellido varchar(45), in contrasena varchar(45), in email varchar(45), in val decimal(3,2), in numeroCel varchar(45))
BEGIN
declare contra varchar(512); 
set contra=sha2(contrasena,512);
 INSERT INTO `mapu`.`users` (`name`, `lastname`, `password`, `email`, `value`, `numberPhone`, `status`) VALUES 
 (nombre, apellido, contra, email, val, numeroCel,1);
END