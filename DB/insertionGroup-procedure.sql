CREATE DEFINER=`root`@`%` PROCEDURE `insertionGroup`(in nombre varchar(45),in clasificacion varchar(45), in pass varchar(45))
BEGIN

    select @consult:=idUser from mapu.users where password=sha2(pass,512);
    INSERT INTO `mapu`.`groups` (`name`, `class`, `status`, `idUser`) VALUES (nombre, clasificacion, '1',@consult); 

END